import React, {useEffect, useMemo, useState} from "react";
import "./style-home.css";
import { gql, useQuery } from "@apollo/client"
import { Bar } from '@visx/shape';
import { Group } from '@visx/group';
import { GradientTealBlue } from '@visx/gradient';
import { scaleBand, scaleLinear } from '@visx/scale';

const monthsMapping = {0:'January', 1:'February', 2:'March', 3: 'April', 4:'May', 5: 'June', 6: 'July', 7:'August', 8: 'September', 9: 'October', 10: 'November', 11: 'December'};
const initialMonths = [
    {id: 1, label: 'January', frequency: 0},
    {id: 2, label: 'February', frequency: 0},
    {id: 3, label: 'March', frequency: 0},
    {id: 4, label: 'April', frequency: 0},
    {id: 5, label: 'May', frequency: 0},
    {id: 6, label: 'June', frequency: 0},
    {id: 7, label: 'July', frequency: 0},
    {id: 8, label: 'August', frequency: 0},
    {id: 9, label: 'September', frequency: 0},
    {id: 10, label: 'October', frequency: 0},
    {id: 11, label: 'November', frequency: 0},
    {id: 12, label: 'December', frequency: 0}
]
const verticalMargin = 120;
const width = 800;
const height = 500;
const events = true;
const xMax = width;
const yMax = height - verticalMargin;

const getLabel = (d) => d.label;
const getLabelFrequency = (d) => Number(d.frequency) ;

const ALL_POSTS = gql`
  query GetPosts {
    allPosts(count: 500) {
      createdAt
    }
  }
`;

export function Home() {
    const [months, setMonths] = useState (initialMonths);
    const { loading, error, data } = useQuery(ALL_POSTS);

    useEffect(() => {
        if(data) {
            const counts = data?.allPosts.reduce((acc, { createdAt:  value }) => {
                let monthNumber = new Date(parseInt(value)).getMonth();
                let year = new Date(parseInt(value)).getFullYear();
                if (year !== 2019) return {...acc};
                return {...acc, [monthsMapping[monthNumber]]: (acc[monthsMapping[monthNumber]] || 0) + 1}
            }, {});

            let newMonthsArray = [...months];
            newMonthsArray.map((month) => {
                month.frequency = counts[month.label];
                return month;
            })
            setMonths(newMonthsArray)
        }
    }, [data])

    const xScale = useMemo(
        () =>
            scaleBand({
                range: [0, xMax],
                round: true,
                domain: months.map(getLabel),
                padding: 0.4,
            }),
        [xMax,months ],
    );
    const yScale = useMemo(
        () =>
            scaleLinear({
                range: [yMax, 0],
                round: true,
                domain: [0, Math.max(...months.map(getLabelFrequency))],
            }),
        [yMax,months],
    );

    if (loading) return (<div className={'content'}><p>Loading...</p></div>);
    if (error) return <p>Error :(</p>;

    return width < 10 ? null : (
        <div className={'content'}>
            <svg width={width} height={height}>
                <GradientTealBlue id="teal" />
                <rect width={width} height={height} fill="url(#teal)" rx={14} />
                <Group top={verticalMargin / 2}>
                    {months.map(d => {
                        const label = getLabel(d);
                        const barWidth = xScale.bandwidth();
                        const barHeight = yMax - (yScale(getLabelFrequency(d)) ?? 0);
                        const barX = xScale(label);
                        const barY = yMax - barHeight;
                        return (
                            <React.Fragment key={`bar-${label}`}>
                                <Bar
                                    key={`bar-${label}`}
                                    x={barX}
                                    y={barY}
                                    width={barWidth}
                                    height={barHeight}
                                    fill="rgba(23, 233, 217, .5)"
                                    onClick={() => {
                                        if (events) alert(`clicked: ${JSON.stringify(Object.values(d))}`);
                                    }}

                                />
                                <text
                                    x={xScale(getLabel(d))}
                                    y={yMax - barHeight}
                                    fill="black"
                                    fontSize={12}
                                    dx={"-.2em"}
                                    dy={"-.33em"}
                                    style={{ fontFamily: "arial" }}
                                >
                                    {`${(getLabelFrequency(d))} posts`}
                                </text>
                                <text
                                    x={xScale(getLabel(d))}
                                    y={yMax}
                                    fill="aqua"
                                    fontSize={12}
                                    dx={"1.5em"}
                                    dy={"1em"}
                                    textAnchor="middle"
                                >
                                    {getLabel(d)}
                                </text>
                            </React.Fragment>
                        );
                    })}
                </Group>
            </svg>
        </div>

    );

}

