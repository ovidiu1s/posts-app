import React from "react";
import { Home } from "./pages/home/Home";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import { ApolloClient, ApolloProvider, HttpLink, InMemoryCache } from "@apollo/client"

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: 'https://fakerql.stephix.uk/graphql',
  }),
  credentials: "same-origin",
})

function AppRouter() {
  return (
      <div id="wrapper">
        <Router>
          <Switch>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </div>
  );
}

function App() {
  return (
      <ApolloProvider client = {client}>
        <AppRouter />
      </ApolloProvider>
  );
}

export default App;
